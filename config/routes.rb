Rails.application.routes.draw do

	root 'pages#home'
	get 'about', to: 'pages#about'
	get 'signup', to: 'users#new'
	resources :users, except: [:new]
	get 'login', to: 'sessions#new'
	post 'login', to: 'sessions#create'
	delete 'logout', to: 'sessions#destroy'

  # all the rest-ful routes (REST = Representational state transfer - mapping HTTP verbs (get, post, put/patch, delete) to CRUD actions)
	# resources :articles, only: [:show, :index, :new, :create, :edit, :update, :destroy]
	resources :articles
  resources :categories, except: [:destroy]


end


