require 'test_helper'

class SignInUserTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.create(username:"marc", email:"marc@example.com", password:"123456", admin: false)
  end

  test "sign in succesfully"  do
    sign_in_as(@user, "123456")
    follow_redirect!
    assert_template 'users/show'    
  end

end