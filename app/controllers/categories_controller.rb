class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update]
  before_action :require_admin, except: [:index, :show]

  def index
    @categories = Category.paginate(page: params[:page])
  end
  
  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      flash[:notice] = "Category was created successfully."
      redirect_to categories_path
    else
      render 'new'
    end
  end

  def edit

  end

  def update
    if @acategory.update(category_params)
      flash[:notice] = "Category was updated successfully."
      redirect_to @category
    else
      render 'edit'
    end
  end

  def show
    @category_articles = @category.articles.paginate(page: params[:page])
  end

  private

  def category_params
    params.require(:category).permit(:name)
  end

  def set_category
      @category = Category.find(params[:id])
  end

  def require_admin
      if !helpers.logged_in? || (helpers.logged_in? and !helpers.current_user.admin?)
        flash[:danger] = "Only admins can perform that action"
        redirect_to categories_path
      end
    end

end