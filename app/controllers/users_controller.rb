class UsersController < ApplicationController

	before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :require_same_user, only: [:edit, :update]
  before_action :require_admin, only: [:destroy]

	def show
		@user_articles = @user.articles.paginate(page: params[:page])
	end

  def destroy
    @user.destroy
    flash[:danger] = "User and all articles created by user have been deleted"
    redirect_to users_path
  end

	def index
		@users = User.paginate(page: params[:page])
	end

	def new
		@user = User.new
	end

	def edit
		
	end


	def create
		@user = User.new(user_params)
		if @user.save
      session[:user_id] = @user.id
			flash[:notice] = "You have signed up to Alpha Blog #{@user.username}"
			redirect_to user_path(@user)
		else
			render 'new'
		end
	end

	def update		
		if @user.update(user_params)
			flash[:notice] = "Your account was updated successfully."
			redirect_to @user
		else
			render 'edit'
		end
	end


	private
	def user_params
		params.require(:user).permit(:username, :email, :password)
	end

	def set_user
		@user = User.find(params[:id])
	end

  def require_same_user
    if helpers.current_user != @user and !helpers.current_user.admin?
      flash[:danger] = "You cannot edit this user"
      redirect_to user_path(@user)
    end
  end

  def require_admin
    if helpers.logged_in and !helpers.current_user.admin?
      flash[:danger] = "You are not allowed to delete users"
      redirect_to users_path
    end
  end
end